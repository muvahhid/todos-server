require('dotenv').config()
const express = require('express')
const app = express()
const cors = require('cors')
const mongoose = require('mongoose')

const router = require('./routes/index')

const db = 'mongodb+srv://astromonaa:todos321@cluster0.i2j4zov.mongodb.net/todosApp?retryWrites=true&w=majority'
const PORT = process.env.PORT || 5000

app.use(express.json())
app.use(cors())

app.use('/api', router)

const start = async () => {
  try {
    await mongoose.connect(db)
    app.listen(PORT, () => console.log(`Litening ${PORT}\nConnected to db`))
  }catch(e) {
    console.log(e);
  }
}
start()