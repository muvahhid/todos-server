const Todo = require('../models/todo')

class TodosService {
  async create(title) {
    const todo = new Todo({title})
    return await todo.save()
  }
  async getAll() {
    const todos = await Todo.find()
    return todos
  }
  async getOne(_id) {
    const find = await Todo.findById(_id)
    return find
  }
  async update(_id, title, completed) {
    const updated = await Todo.findByIdAndUpdate({_id}, {title, completed}, {new: true})
    return updated
  }
  async delete(_id) {
    const deleted = await Todo.findOneAndDelete({_id})
    return deleted
  }
}

module.exports = new TodosService