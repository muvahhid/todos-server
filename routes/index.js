const router = require('express').Router()
const TodosController = require('../Controllers/TodosController')

router.get('/todos', TodosController.getAll)
router.get('/todos/:_id', TodosController.getOne)
router.post('/todos', TodosController.create)
router.put('/todos/:_id', TodosController.editTodo)
router.delete('/todos/:_id', TodosController.delete)

module.exports = router