const TodosService = require('../services/TodosService')

class TodosController {
  async create(req, res, next) {
    try {
      const { title } = req.body;
      const todo = await TodosService.create(title)
      await res.json(todo)
    }catch(e) {
      next(e)
    }
  }
  async getAll(req, res, next) {
    try {
      const todos = await TodosService.getAll()
      await res.json(todos)
    }catch(e) {
      netx(e)
    }
  }
  async getOne(req, res, next) {
    try {
      const { _id } = req.params
      const find = await TodosService.getOne(_id)
      await res.json(find)
    }catch(e) {
      next(e)
    }
  }
  async editTodo(req, res, next) {
    try {
      const { title, completed } = req.body
      const { _id } = req.params
      const updated = await TodosService.update(_id, title, completed)
      await res.json(updated)
    }catch(e) {
      next(e)
    }
  }
  async delete (req, res, next) {
    try {
      const { _id } = req.params
      const deleted = await TodosService.delete(_id)
      await res.json(deleted)
    }catch(e) {
      next(e)
    }
  }
}
module.exports = new TodosController()